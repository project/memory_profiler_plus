<?php

namespace Drupal\memory_profiler_plus\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Subscribes to the kernel request event to register a shutdown function.
 */
class MemoryProfilerPlusEventSubscriber implements EventSubscriberInterface {

  /**
   * Register memory_profiler_plus_shutdown function.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event to process.
   */
  public function onRequest(RequestEvent $event) {
    drupal_register_shutdown_function('memory_profiler_plus_shutdown');
  }

  /**
   * Sets memory-peak header.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event.
   */
  public function onResponse(ResponseEvent $event) {

    if (!$event->isMainRequest()) {
      return;
    }

    $response = $event->getResponse();

    $memory_peak = round(memory_get_peak_usage(TRUE) / 1024 / 1024, 2);
    $message = "$memory_peak MB";

    $response->headers->set("memory-peak", $message);
  }

  /**
   * Implements EventSubscriberInterface::getSubscribedEvents().
   *
   * @return array
   *   An array of event listener definitions.
   */
  public static function getSubscribedEvents() {
    // Set a low value to start as early as possible.
    $events[KernelEvents::REQUEST][] = ['onRequest', -100];
    $events[KernelEvents::RESPONSE][] = ['onResponse', -100];

    return $events;
  }

}
