<?php

namespace Drupal\memory_profiler_plus\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MemoryProfilerPlusSettingsForm. Settings form for the memory profiler plus module
 *
 * @package Drupal\memory_profiler_plus\Form
 */
class MemoryProfilerPlusSettingsForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'memory_profiler_plus_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['memory_profiler_plus.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('memory_profiler_plus.settings');

    $form['profiling_enabled']= [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable profiling of requests?'),
      '#description' => $this->t('Enables or disables the profiling of requests. If enabled each request will perform a single upsert in the database table.'),
      '#default_value' => $config->get('profiling_enabled'),
    ];

    $form['actions']['truncate_submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clear profiling data'),
      '#submit' => ['::truncateSubmit'],
    ];

    $message = $this->t("'Clear profiling data' removes all existing profiling info from the database table. There is no confirmation and no undo.");

    $form['truncate_description'] = [
      '#markup' => '<div id="edit-profiling-enabled--description" class="form-item__description">' . $message . '</div>', 
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('memory_profiler_plus.settings')
      ->set('profiling_enabled', $form_state->getValue('profiling_enabled'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  public function truncateSubmit(array &$form, FormStateInterface $form_state) {

    \Drupal::database()->truncate('memory_profiler_plus_log')->execute();

    $this->messenger()->addStatus(t('All request profiling data has been deleted.'));
  }
}
