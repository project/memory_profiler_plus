README.txt
==========

A module for profiling memory usage. 

Memory usage on each path is logged to a database table where the count of request for each path, 
along with the minimum, maximum and last request time values will be recorded.

This module is entirely based on the original Drupal memory_profiler module created by
Drew Webber (mcdruid).

https://www.drupal.org/project/memory_profiler

The original module logs profile data to the Drupal DBLog or syslog (if your site is so
configured) which can be limiting if you are not using syslog and have to rely on DBLog
to see whats going on.

Without being able to generate reports from syslog you are limited to seeing individual log
entries in the DB Log and you might have that disabled or limited to a low number of entries.
In any case having profiling requests clutter up your DBLog is not ideal.

Module features
===============
* Logs profiling data to a database table.
* A view is provided for examining profiling data.
* Profiling can be enabled or disabled from settings.
* Profiling data can be truncated from the settings dialog.

In addition the module retains the originals functionality of adding the memory used by the request to
the response header.

Consideratons
=============
* This module adds a DB upsert to every request that Drupal handles so does add some overhead. A single page load can be many requests so you should consider the impact it may have on any site you use it on. As very rough guidance - On a 13th Gen Intel(R) Core(TM) i7-13700H, 2400 Mhz, with 14 Core(s) and 20 Logical Processor(s) running within a DDEV based environment under Windws/WSL2 an insert rate of 8000 records/second was achieved.
* If at all possible you should always run this on a non production environment or if needs must run it for the minimal amount of time neccessary to get the information you need. This is easy to do by installing the module, enabling profiling in the settings for a short period and then disabling profiling. You can then use the report view to examine the data.
